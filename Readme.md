# Zopa Technical Test {Spring boot application }

## Assignment 
There is a need for a rate calculation system allowing prospective borrowers to
 obtain a quote from our pool of lenders for 36 month loans. This system will 
 take the form of a command-line application.
 
 You will be provided with a file containing a list of all the offers being made
 by the lenders within the system in CSV format, see the example market.csv file
 provided alongside this specification.
 
 You should strive to provide as low a rate to the borrower as is possible to
 ensure that Zopa's quotes are as competitive as they can be against our
 competitors'. You should also provide the borrower with the details of the
 monthly repayment amount and the total repayment amount.
 
 Repayment amounts should be displayed to 2 decimal places and the rate of the 
 loan should be displayed to one decimal place.
 
 Borrowers should be able to request a loan of any £100 increment between £1000
 and £15000 inclusive. If the market does not have sufficient offers from
 lenders to satisfy the loan then the system should inform the borrower that it
 is not possible to provide a quote at that time.
 


## Assumptions
*  Yearly rate of interest is in the excel.


## Prerequisites
Install the following tools

* [Java 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 
   - Lambda and Annotation were used in the code
* [Gradle](https://gradle.org/) - Dependency Management

## Building the code:
* Clone from Bitbucket or unzip the supplied zip file
* Navigate to the project directory in parallel to build.gradle.

```
gradlew clean build
```

```
From the root path, run the following commands:

java -jar build/libs/com.zopa.loan-0.0.1-SNAPSHOT.jar Market.csv 1000
```

## TOTO
* Mocking framework can be used in test or Spring Test framework can be also used.
* Logging can be improved. 
* Error message cab be configurable.
 