package com.zopa.loan.modal;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class LoanPaymentQuote {
    private BigDecimal requestedAmount;
    private Double interestRate;
    private BigDecimal monthlyRepayment;
    private BigDecimal totalRepayment;

    public void printFormattedResult() {
        System.out.println("Request Amount: £" +
                String.format("%.0f", getRequestedAmount()));
        System.out.println("Rate: " +
                String.format("%.1f", getInterestRate() * 100) + "%");
        System.out.println("Monthly repayment £" +
                String.format("%.2f", getMonthlyRepayment()));
        System.out.println("Total repayment: £" +
                String.format("%.2f", getTotalRepayment()));

    }
}
