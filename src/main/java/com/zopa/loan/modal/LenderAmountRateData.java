package com.zopa.loan.modal;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class LenderAmountRateData {
    private String lenderName;
    private Double interestRate;
    private BigDecimal availableAmount;
}
