package com.zopa.loan;


import com.zopa.loan.modal.LoanPaymentQuote;
import com.zopa.loan.service.PaymentCalculator;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@EnableAutoConfiguration
@SpringBootApplication
public class ZopaLoanApplication implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(ZopaLoanApplication.class);


    @Autowired
    @Qualifier("paymentCalculator")
    PaymentCalculator paymentCalculator;


    @Override
    public void run(String... args) throws Exception {
        LoanPaymentQuote loanPaymentQuote = paymentCalculator.calculateRate();
        loanPaymentQuote.printFormattedResult();
    }

    public static void main(String[] args) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Starring the {} application", ZopaLoanApplication.class);
        }
        if (args.length != 2) {
            Assert.fail("Invalid number of command line parameter");
        }

        System.setProperty("file.filePath", args[0]);
        System.setProperty("requestedAmount", args[1]);


        new SpringApplicationBuilder(ZopaLoanApplication.class)
                .logStartupInfo(false)
                .bannerMode(Banner.Mode.OFF)
                .run(args);


    }


}
