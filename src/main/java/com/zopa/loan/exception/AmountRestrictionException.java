package com.zopa.loan.exception;

/**
 * if the requested loan amount is not valid in the valid ranges
 * Business Exception- category
 *
 * @author jyotipoddar
 */
public class AmountRestrictionException extends RuntimeException {

    public AmountRestrictionException(String message) {
        super(message);
    }
}
