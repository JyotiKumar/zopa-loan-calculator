package com.zopa.loan.exception;

/**
 * Low total available balance exception
 * Business Exception- category
 *
 * @author jyotipoddar
 */
public class LowAvailableBalanceException extends RuntimeException {

    public LowAvailableBalanceException(String message) {
        super(message);
    }
}
