package com.zopa.loan.exception;

/**
 * This exception class for the error message
 * if the input file can't read.
 * if the requested loan amount is not valid.
 *
 * @author jyotipoddar
 */
public class InvalidArgumentException extends RuntimeException {

    public InvalidArgumentException(String message) {
        super(message);
    }
}
