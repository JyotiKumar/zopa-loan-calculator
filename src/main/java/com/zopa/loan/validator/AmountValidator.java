package com.zopa.loan.validator;


/**
 * @author jyotipoddar
 */
public interface AmountValidator {

    Double MIN_LOAN_AMOUNT = Double.valueOf(1000);
    Double MAX_LOAN_AMOUNT = Double.valueOf(15000);
    Double INCREMENT_AMOUNT = Double.valueOf(100);

    /**
     * Method to validate the amount is in valid range or not.
     *
     * @param amount
     * @return boolean
     */
    boolean validateAmountRange(Double amount);

    /**
     * Method to validate the amount is with valid increment.
     *
     * @param amount
     * @return boolean
     */
    boolean validateIncrementAmount(Double amount);


}
