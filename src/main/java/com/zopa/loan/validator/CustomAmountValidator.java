package com.zopa.loan.validator;

public class CustomAmountValidator implements AmountValidator {

    @Override
    public boolean validateAmountRange(Double amount) {
        return !(amount >= MIN_LOAN_AMOUNT && amount <= MAX_LOAN_AMOUNT);
    }

    @Override
    public boolean validateIncrementAmount(Double amount) {
        return !(amount % INCREMENT_AMOUNT == 0);
    }
}
