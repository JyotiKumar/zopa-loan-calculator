package com.zopa.loan.validator;

import java.nio.file.Paths;

public class StandardInputArgumentValidator implements InputArgumentValidator {
    @Override
    public boolean isFileExists(String fileName) {
        return !Paths.get(fileName).toFile().exists();
    }
}
