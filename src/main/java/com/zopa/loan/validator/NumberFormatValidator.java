package com.zopa.loan.validator;

@FunctionalInterface
public interface NumberFormatValidator {

    /**
     * Method to check is the file is accessed or exits on the given path.
     *
     * @param amount amount
     * @return false if input string is a valid amount.
     */

    boolean isValidAmountFormat(String amount);
}
