package com.zopa.loan.validator;

/**
 * interface to validate input arguments.
 *
 * @author jyotipoddar
 */

@FunctionalInterface
public interface InputArgumentValidator {

    /**
     * Method to check is the file is accessed or exits on the given path.
     *
     * @param fileName fileName
     * @return true if file exists else false
     */
    boolean isFileExists(String fileName);


}
