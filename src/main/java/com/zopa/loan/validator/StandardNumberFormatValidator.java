package com.zopa.loan.validator;

public class StandardNumberFormatValidator implements NumberFormatValidator {

    @Override
    public boolean isValidAmountFormat(String amount) {
        boolean isValidNumber = true;
        try {
            Double.valueOf(amount);

        } catch (NumberFormatException ex) {
            isValidNumber = false;
        }
        return !isValidNumber;
    }
}
