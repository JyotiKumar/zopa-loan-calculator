package com.zopa.loan.service.impl;

import com.zopa.loan.exception.AmountRestrictionException;
import com.zopa.loan.exception.InvalidArgumentException;
import com.zopa.loan.exception.LowAvailableBalanceException;
import com.zopa.loan.modal.LenderAmountRateData;
import com.zopa.loan.modal.LoanPaymentQuote;
import com.zopa.loan.service.DataFileProcessor;
import com.zopa.loan.service.MonthlyPaymentCalculator;
import com.zopa.loan.service.PaymentCalculator;
import com.zopa.loan.validator.AmountValidator;
import com.zopa.loan.validator.CustomAmountValidator;
import com.zopa.loan.validator.InputArgumentValidator;
import com.zopa.loan.validator.StandardInputArgumentValidator;
import com.zopa.loan.validator.StandardNumberFormatValidator;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


public class PaymentCalculatorService implements PaymentCalculator {

    private final String fileNameOrNameWithPath;
    private final String loanAmount;
    private final InputArgumentValidator standardInputArgumentValidator;
    private final AmountValidator customAmountValidator;
    private final DataFileProcessor lenderDataFileProcessorService;
    private final MonthlyPaymentCalculator standardMonthlyPaymentCalculatorService;


    public PaymentCalculatorService(String fileNameOrNameWithPath,
                                    String loanAmount,
                                    InputArgumentValidator standardInputArgumentValidator,
                                    AmountValidator customAmountValidator,
                                    DataFileProcessor lenderDataFileProcessorService,
                                    MonthlyPaymentCalculator standardMonthlyPaymentCalculatorService) {
        this.fileNameOrNameWithPath = fileNameOrNameWithPath;
        this.loanAmount = loanAmount;
        this.standardInputArgumentValidator = standardInputArgumentValidator;
        this.customAmountValidator = customAmountValidator;
        this.lenderDataFileProcessorService = lenderDataFileProcessorService;
        this.standardMonthlyPaymentCalculatorService = standardMonthlyPaymentCalculatorService;
    }

    @Override
    public LoanPaymentQuote calculateRate() throws IOException {
        if (standardInputArgumentValidator.isFileExists(fileNameOrNameWithPath)) {
            throw new InvalidArgumentException("Unable to read the file: " + fileNameOrNameWithPath);
        } else if (new StandardNumberFormatValidator().isValidAmountFormat(loanAmount)) {
            throw new InvalidArgumentException("Argument is not valid amount: " + loanAmount);
        }

        final Double requestedAmount = Double.valueOf(loanAmount);
        if (customAmountValidator.validateAmountRange(requestedAmount)
                && customAmountValidator.validateIncrementAmount(requestedAmount)) {
            throw new AmountRestrictionException("Requested amount " +
                    "is not in valid range or in valid increment");
        }
        final List<LenderAmountRateData> lenderAmountRateDataList = lenderDataFileProcessorService.
                readAndProcessFile(fileNameOrNameWithPath);

        final List<LenderAmountRateData> selectedLenders = standardMonthlyPaymentCalculatorService.
                getSelectedListOfLenders(lenderAmountRateDataList, loanAmount);

        if (selectedLenders.size() > 0) {
            final BigDecimal monthlyPayment = selectedLenders.stream().map(LenderAmountRateData::getAvailableAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, RoundingMode.HALF_UP);
            if (monthlyPayment.compareTo(BigDecimal.valueOf(requestedAmount)) == -1) {
                throw new LowAvailableBalanceException("Low total lenders Available balance");
            }
        }

        final LoanPaymentQuote loanPaymentQuote = standardMonthlyPaymentCalculatorService.
                getMonthlyPaymentQuote(selectedLenders, BigDecimal.valueOf(Double.valueOf(loanAmount)));
        return loanPaymentQuote;
    }


}
