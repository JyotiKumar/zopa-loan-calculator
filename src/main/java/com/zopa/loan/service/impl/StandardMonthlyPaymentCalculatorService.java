package com.zopa.loan.service.impl;

import com.zopa.loan.modal.LenderAmountRateData;
import com.zopa.loan.modal.LoanPaymentQuote;
import com.zopa.loan.service.MonthlyPaymentCalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

public class StandardMonthlyPaymentCalculatorService implements MonthlyPaymentCalculator {


    @Override
    public LoanPaymentQuote getMonthlyPaymentQuote(final List<LenderAmountRateData> selectedLenders,
                                                   BigDecimal loanAmount) {
        final List<BigDecimal> monthlyPayments = new ArrayList<>();
        selectedLenders.forEach(lenderOffer -> {
            double monthlyRate = lenderOffer.getInterestRate() / 12;
            BigDecimal monthlyPayment = lenderOffer.getAvailableAmount().multiply(BigDecimal.valueOf(monthlyRate)).multiply(BigDecimal.valueOf(Math.pow(1 + monthlyRate, TOTAL_MONTH)))
                    .divide(BigDecimal.valueOf(Math.pow(1 + monthlyRate, TOTAL_MONTH) - 1), RoundingMode.HALF_UP);
            monthlyPayments.add(monthlyPayment);

        });
        final BigDecimal monthlyPayment = monthlyPayments.stream().reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, RoundingMode.HALF_UP);
        final BigDecimal totalRePaymentAmount = monthlyPayment.multiply(BigDecimal.valueOf(TOTAL_MONTH))
                .setScale(2, RoundingMode.HALF_UP);
        final OptionalDouble optionalInterestRate = selectedLenders.stream().
                mapToDouble(LenderAmountRateData::getInterestRate).average();
        Double interestRate = null;
        if (optionalInterestRate.isPresent()) {
            interestRate = optionalInterestRate.getAsDouble();
        }
        return LoanPaymentQuote.builder()
                .interestRate(Math.round(interestRate * 1000.0) / 1000.0)
                .monthlyRepayment(monthlyPayment)
                .totalRepayment(totalRePaymentAmount)
                .requestedAmount(loanAmount)
                .build();
    }


    @Override
    public List<LenderAmountRateData> getSelectedListOfLenders(List<LenderAmountRateData>
                                                                       lendersAmountRateData, String loanAmount) {
        final List<LenderAmountRateData> selectedLenders = new ArrayList<>();
        BigDecimal totalLoanAmount = new BigDecimal(loanAmount);

        // filter out list of all selected lenders
        for (LenderAmountRateData lenderAmountRateData : lendersAmountRateData) {
            int result = totalLoanAmount.compareTo(lenderAmountRateData.getAvailableAmount());
            if (result == 0) { // total amount is equal to the amount
                selectedLenders.add(lenderAmountRateData);
                break;
            } else if (result == 1) { // greater
                selectedLenders.add(lenderAmountRateData);
                totalLoanAmount = totalLoanAmount.subtract(lenderAmountRateData.getAvailableAmount());

            } else if (result == -1) {  // less
                lenderAmountRateData.setAvailableAmount(totalLoanAmount);
                selectedLenders.add(lenderAmountRateData);
                break;
            }
        }
        return selectedLenders;
    }
}