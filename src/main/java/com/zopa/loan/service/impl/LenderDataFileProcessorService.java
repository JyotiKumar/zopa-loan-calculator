package com.zopa.loan.service.impl;

import com.zopa.loan.modal.LenderAmountRateData;
import com.zopa.loan.service.DataFileProcessor;
import com.zopa.loan.validator.StandardNumberFormatValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LenderDataFileProcessorService implements DataFileProcessor {


    private static final Logger LOG = LoggerFactory.getLogger(LenderDataFileProcessorService.class);

    private final StandardNumberFormatValidator numberValidator;

    public LenderDataFileProcessorService(StandardNumberFormatValidator numberValidator) {
        this.numberValidator = numberValidator;
    }


    @Override
    public List<LenderAmountRateData> readAndProcessFile(String fileNameOrFileWithPath) throws IOException {
        List<LenderAmountRateData> lenderAmountRateDataList = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileNameOrFileWithPath))) {
            lenderAmountRateDataList = stream.skip(1)
                    .filter(line -> line.contains(","))
                    .map(line -> line.split(","))
                    .filter(val -> (!(numberValidator.isValidAmountFormat(val[1]))
                            || numberValidator.isValidAmountFormat(val[2])))
                    .map(lineData -> LenderAmountRateData.builder()
                            .lenderName(lineData[0])
                            .interestRate(Double.valueOf(lineData[1]))
                            .availableAmount(new BigDecimal(Double.valueOf(lineData[2]))).build())
                    .filter(lenderAmountRateData ->
                            lenderAmountRateData.getAvailableAmount().longValue() > 0
                                    && lenderAmountRateData.getInterestRate() > 0
                    ).sorted(Comparator.comparingDouble(LenderAmountRateData::getInterestRate)).collect(Collectors.toList());
        }
        return lenderAmountRateDataList;
    }

}
