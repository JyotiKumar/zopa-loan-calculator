package com.zopa.loan.service;

import com.zopa.loan.modal.LoanPaymentQuote;

import java.io.IOException;


public interface PaymentCalculator {
    LoanPaymentQuote calculateRate() throws IOException;
}
