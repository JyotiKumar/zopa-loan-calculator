package com.zopa.loan.service;


import com.zopa.loan.modal.LenderAmountRateData;

import java.io.IOException;
import java.util.List;

public interface DataFileProcessor<LenderAmountRateData> {
    List<com.zopa.loan.modal.LenderAmountRateData> readAndProcessFile(String fileNameOrFileWithPath) throws IOException;
}
