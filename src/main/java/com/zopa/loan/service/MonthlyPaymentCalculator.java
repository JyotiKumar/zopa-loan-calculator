package com.zopa.loan.service;

import com.zopa.loan.modal.LenderAmountRateData;
import com.zopa.loan.modal.LoanPaymentQuote;

import java.math.BigDecimal;
import java.util.List;

public interface MonthlyPaymentCalculator {


    int TOTAL_MONTH = 36;


    /**
     * Method to get list of selected lenders in
     * contributing the total amount.
     *
     * @param lendersAmountRateData
     * @param loanAmount            loan amount
     * @return List of selected LenderAmountRateData
     */
    List<LenderAmountRateData> getSelectedListOfLenders(final List<LenderAmountRateData> lendersAmountRateData,
                                                        String loanAmount);

    /**
     * Method to get the Monthly payment quote details
     *
     * @param lendersAmountRateData
     * @param loanAmount
     * @return
     */
    LoanPaymentQuote getMonthlyPaymentQuote(final List<LenderAmountRateData> lendersAmountRateData,
                                            BigDecimal loanAmount);
}
