package com.zopa.loan.spring.config;


import com.zopa.loan.service.DataFileProcessor;
import com.zopa.loan.service.MonthlyPaymentCalculator;
import com.zopa.loan.service.PaymentCalculator;
import com.zopa.loan.service.impl.LenderDataFileProcessorService;
import com.zopa.loan.service.impl.PaymentCalculatorService;
import com.zopa.loan.service.impl.StandardMonthlyPaymentCalculatorService;
import com.zopa.loan.validator.AmountValidator;
import com.zopa.loan.validator.CustomAmountValidator;
import com.zopa.loan.validator.StandardInputArgumentValidator;
import com.zopa.loan.validator.StandardNumberFormatValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class AppConfiguration {

    @Autowired
    private Environment environment;


    @Bean
    public DataFileProcessor dataFileProcessor() {
        return new LenderDataFileProcessorService(standardNumberFormatValidator());
    }


    @Bean
    public MonthlyPaymentCalculator monthlyPaymentCalculator() {
        return new StandardMonthlyPaymentCalculatorService();
    }

    @Bean
    @Qualifier("standardNumberFormatValidator")
    public StandardNumberFormatValidator standardNumberFormatValidator() {

        return new StandardNumberFormatValidator();
    }

    @Bean
    @Qualifier("standardInputArgumentValidator")
    public StandardInputArgumentValidator standardInputArgumentValidator() {

        return new StandardInputArgumentValidator();
    }

    @Bean
    public AmountValidator customAmountValidator() {
        return new CustomAmountValidator();
    }

    @Bean
    @Qualifier("paymentCalculator")
    public PaymentCalculator paymentCalculator() {
        return new PaymentCalculatorService(environment.getProperty("file.filePath"),
                environment.getProperty("requestedAmount"), standardInputArgumentValidator(),
                customAmountValidator(), dataFileProcessor(), monthlyPaymentCalculator());
    }

}
