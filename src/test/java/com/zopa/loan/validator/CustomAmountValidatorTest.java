package com.zopa.loan.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CustomAmountValidatorTest {

    CustomAmountValidator customAmountValidator;

    @Before
    public void setUp() throws Exception {
        customAmountValidator = new CustomAmountValidator();
    }

    @Test
    public void validateAmountRange() {
        Assert.assertFalse(customAmountValidator.validateAmountRange(Double.valueOf(1000)));
    }

    @Test
    public void validateAmountRange_invalid() {
        Assert.assertTrue(customAmountValidator.validateAmountRange(Double.valueOf(100)));
    }

    @Test
    public void validateIncrementAmount() {
        Assert.assertFalse(customAmountValidator.validateIncrementAmount(Double.valueOf(100)));
    }

    @Test
    public void validateIncrementAmount_Invlid() {
        Assert.assertTrue(customAmountValidator.validateIncrementAmount(Double.valueOf(2)));
    }
}