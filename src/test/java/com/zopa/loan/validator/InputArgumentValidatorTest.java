package com.zopa.loan.validator;

import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Paths;

public class InputArgumentValidatorTest {

    @Test
    public void isFileExists() {
        Assert.assertTrue(Paths.get("market.csv").toFile().exists());
    }
    @Test
    public void isFileExists_invalid() {
        Assert.assertFalse(Paths.get("marketData.csv").toFile().exists());
    }
}