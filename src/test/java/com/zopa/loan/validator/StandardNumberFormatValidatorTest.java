package com.zopa.loan.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StandardNumberFormatValidatorTest {

    StandardNumberFormatValidator numberFormatValidator;

    @Before
    public void setUp() throws Exception {
        numberFormatValidator = new StandardNumberFormatValidator();
    }

    @Test
    public void isValidAmountFormat() {
        Assert.assertFalse(numberFormatValidator.isValidAmountFormat("100"));
    }


    @Test
    public void isValidAmountFormat_invalid() {
        Assert.assertTrue(numberFormatValidator.isValidAmountFormat("100AA"));
    }
}