package com.zopa.loan.service.impl;

import com.zopa.loan.modal.LenderAmountRateData;
import com.zopa.loan.validator.StandardNumberFormatValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class LenderDataFileProcessorServiceTest {

    public LenderDataFileProcessorService lenderDataFileProcessorService;

    final String fileName = "Market.csv";

    @Before
    public void setUp() throws Exception {
        lenderDataFileProcessorService = new LenderDataFileProcessorService(new StandardNumberFormatValidator());
    }

    @Test
    public void readAndProcessFile() throws IOException {
        List<LenderAmountRateData> lendersAmountRateData = lenderDataFileProcessorService.readAndProcessFile(fileName);
        Assert.assertTrue("The fil size is 7", lendersAmountRateData.size() == 7);

    }

    @Test(expected = IOException.class)
    public void readIfNotValid() throws IOException {
        List<LenderAmountRateData> lendersAmountRateData = lenderDataFileProcessorService.readAndProcessFile("test.csv");
    }
}