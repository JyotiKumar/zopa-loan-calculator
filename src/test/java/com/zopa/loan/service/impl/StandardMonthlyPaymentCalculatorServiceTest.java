package com.zopa.loan.service.impl;

import com.zopa.loan.modal.LenderAmountRateData;
import com.zopa.loan.modal.LoanPaymentQuote;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StandardMonthlyPaymentCalculatorServiceTest {

    StandardMonthlyPaymentCalculatorService standardMonthlyPaymentCalculatorService;
    LoanPaymentQuote loanPaymentQuote;

    @Before
    public void setUp() throws Exception {
        standardMonthlyPaymentCalculatorService = mock(StandardMonthlyPaymentCalculatorService.class);
        loanPaymentQuote = LoanPaymentQuote.builder().
                interestRate(7.0)
                .monthlyRepayment(BigDecimal.valueOf(30.88))
                .requestedAmount(BigDecimal.valueOf(1000))
                .totalRepayment(BigDecimal.valueOf(1111.68)).build();
    }


    @Test
    public void getMonthlyPaymentQuote() {
        List<LenderAmountRateData> lendersAmountRateData = Mockito.anyListOf(LenderAmountRateData.class);
        when(standardMonthlyPaymentCalculatorService
                .getMonthlyPaymentQuote(lendersAmountRateData, Mockito.any(BigDecimal.class))).thenReturn(loanPaymentQuote);
        assertEquals(loanPaymentQuote.getInterestRate(), Double.valueOf(7.0));
        assertEquals(loanPaymentQuote.getRequestedAmount(), BigDecimal.valueOf(1000));
        assertEquals(loanPaymentQuote.getMonthlyRepayment(), BigDecimal.valueOf(30.88));
        assertEquals(loanPaymentQuote.getTotalRepayment(), BigDecimal.valueOf(1111.68));

    }

    @Test
    public void getSelectedListOfLenders() {
        List<LenderAmountRateData> lendersAmountRateData = Mockito.anyListOf(LenderAmountRateData.class);
        List<LenderAmountRateData> lenders = new ArrayList<>();
        lenders.add(LenderAmountRateData.builder().availableAmount(BigDecimal.valueOf(100))
                .interestRate(Double.valueOf(100))
                .lenderName("Bob").build());
        when(standardMonthlyPaymentCalculatorService
                .getSelectedListOfLenders(lendersAmountRateData, eq("1000")))
                .thenReturn(lenders);

        Assert.assertFalse(lenders.isEmpty());

    }
}