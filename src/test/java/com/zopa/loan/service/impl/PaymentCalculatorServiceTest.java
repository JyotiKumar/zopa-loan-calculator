package com.zopa.loan.service.impl;

import com.zopa.loan.modal.LoanPaymentQuote;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PaymentCalculatorServiceTest {

    LoanPaymentQuote loanPaymentQuote;

    @Before
    public void setUp() throws Exception {
        loanPaymentQuote = LoanPaymentQuote.builder().
                interestRate(7.0)
                .monthlyRepayment(BigDecimal.valueOf(30.88))
                .requestedAmount(BigDecimal.valueOf(1000))
                .totalRepayment(BigDecimal.valueOf(1111.68)).build();

    }


    @Test
    public void calculateRate() throws IOException {
        PaymentCalculatorService paymentCalculatorService = mock(PaymentCalculatorService.class);
        when(paymentCalculatorService.calculateRate()).thenReturn(loanPaymentQuote);
        assertEquals(loanPaymentQuote.getInterestRate(), Double.valueOf(7.0));
        assertEquals(loanPaymentQuote.getRequestedAmount(), BigDecimal.valueOf(1000));
        assertEquals(loanPaymentQuote.getMonthlyRepayment(), BigDecimal.valueOf(30.88));
        assertEquals(loanPaymentQuote.getTotalRepayment(), BigDecimal.valueOf(1111.68));
    }
}